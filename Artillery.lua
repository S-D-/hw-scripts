HedgewarsScriptLoad("/Scriprs/Locale.lua")

local sineTab = {
   0, 8726, 17450, 26168, 34878, 43578, 52264, 60935, 69587, 78217,
   86824, 95404, 103956, 112476, 120961, 129410, 137819, 146186, 154508, 162784,
   171010, 179184, 187303, 195366, 203368, 211309, 219186, 226995, 234736, 242405,
   250000, 257519, 264960, 272320, 279596, 286788, 293893, 300908, 307831, 314660,
   321394, 328030, 334565, 340999, 347329, 353553, 359670, 365677, 371572, 377355,
   383022, 388573, 394005, 399318, 404508, 409576, 414519, 419335, 424024, 428584,
   433013, 437310, 441474, 445503, 449397, 453154, 456773, 460252, 463592, 466790,
   469846, 472759, 475528, 478152, 480631, 482963, 485148, 487185, 489074, 490814,
   492404, 493844, 495134, 496273, 497261, 498097, 498782, 499315, 499695, 499924,
   500000, 499924, 499695, 499315, 498782, 498097, 497261, 496273, 495134, 493844,
   492404, 490814, 489074, 487185, 485148, 482963, 480631, 478152, 475528, 472759,
   469846, 466790, 463592, 460252, 456773, 453154, 449397, 445503, 441474, 437310,
   433013, 428584, 424024, 419335, 414519, 409576, 404508, 399318, 394005, 388573,
   383022, 377355, 371572, 365677, 359670, 353553, 347329, 340999, 334565, 328030,
   321394, 314660, 307831, 300908, 293893, 286788, 279596, 272320, 264960, 257519,
   250000, 242405, 234736, 226995, 219186, 211309, 203368, 195366, 187303, 179184,
   171010, 162784, 154508, 146186, 137819, 129410, 120961, 112476, 103956, 95404,
   86824, 78217, 69587, 60935, 52264, 43578, 34878, 26168, 17450, 8726,
   0, -8726, -17450, -26168, -34878, -43578, -52264, -60935, -69587, -78217,
      -86824, -95404, -103956, -112476, -120961, -129410, -137819, -146186, -154508, -162784,
      -171010, -179184, -187303, -195366, -203368, -211309, -219186, -226995, -234736, -242405,
      -250000, -257519, -264960, -272320, -279596, -286788, -293893, -300908, -307831, -314660,
      -321394, -328030, -334565, -340999, -347329, -353553, -359670, -365677, -371572, -377355,
      -383022, -388573, -394005, -399318, -404508, -409576, -414519, -419335, -424024, -428584,
      -433013, -437310, -441474, -445503, -449397, -453154, -456773, -460252, -463592, -466790,
      -469846, -472759, -475528, -478152, -480631, -482963, -485148, -487185, -489074, -490814,
      -492404, -493844, -495134, -496273, -497261, -498097, -498782, -499315, -499695, -499924,
      -500000, -499924, -499695, -499315, -498782, -498097, -497261, -496273, -495134, -493844,
      -492404, -490814, -489074, -487185, -485148, -482963, -480631, -478152, -475528, -472759,
      -469846, -466790, -463592, -460252, -456773, -453154, -449397, -445503, -441474, -437310,
      -433013, -428584, -424024, -419335, -414519, -409576, -404508, -399318, -394005, -388573,
      -383022, -377355, -371572, -365677, -359670, -353553, -347329, -340999, -334565, -328030,
      -321394, -314660, -307831, -300908, -293893, -286788, -279596, -272320, -264960, -257519,
      -250000, -242405, -234736, -226995, -219186, -211309, -203368, -195366, -187303, -179184,
      -171010, -162784, -154508, -146186, -137819, -129410, -120961, -112476, -103956, -95404,
      -86824, -78217, -69587, -60935, -52264, -43578, -34878, -26168, -17450, -8726,
   0
}
 
local hedgehogs = {}
local prevHog = nil
 
function onGameInit()
   TurnTime = 7000
   Ready = 7000
   EnableGameFlags(gfResetWeps, gfInfAttack, gfArtillery, gfVampiric)
   HealthCaseProb = 0
end
 
function onAmmoStoreInit()
   SetAmmo(amSkip, 9, 0, 0, 0)
   SetAmmo(amJetpack, 1, 0, 0, 0)
   SetAmmo(amBazooka, 9, 0, 0, 0)
   SetAmmo(amTeleport, 1, 0, 0, 0)
   SetAmmo(amRubber, 1, 0, 0, 0)
end
 
function onGearAdd(gear)
   local gearType = GetGearType(gear)
   if gearType == gtJetpack then
      SetHealth(gear, 0)
   elseif gearType == gtTeleport then
      TurnTimeLeft = 1
   elseif gearType == gtHedgehog then
      hedgehogs[gear] = {}
      hedgehogs[gear].hogsKilled = 0
      hedgehogs[gear].gotExtraPower = false
   end
end
 
function onGameTick()
end  
function onSpritePlacement(spriteId, centerX, centerY)
   --AddGear(centerX, centerY, gtNapalmBomb, 0, 0, 0, 0)
end
 
function addFlame(x, y, dx, dy)
 
   local hog = hedgehogs[CurrentHedgehog]
 
   if hog.hogsKilled == 0 then
      return
   end
 
   for i = 0, 360, 5 / hog.hogsKilled do
      local flame = AddGear(x, y, gtFlame, 0,
                            hog.hogsKilled * sineTab[math.floor((i+90)%360+1)],
                            hog.hogsKilled * sineTab[math.floor(i+1)], 0)
      SetHealth(flame, hog.hogsKilled)
      -- AddGear(x, y, gtSineGunShot, 0,
      --                       math.cos(math.rad(i)) * 500000 * hog.hogsKilled,
      --                       500000 * hog.hogsKilled * math.sin(math.rad(i)), 0)
   end
 
   -- if hog.gotExtraPower then
   --    for i = 0, 360, 45 do
   --       AddGear(x, y, gtCluster, 0,
   --               math.cos(math.rad(i)) * 100000 * hog.hogsKilled,
   --               100000 * hog.hogsKilled * math.sin(math.rad(i)), 5)
   --    end
   -- end
   
end
 
function onGearDelete(gear)
   if GetGearType(gear) == gtShell then
      local dx, dy = GetGearVelocity(gear)
      addFlame(GetX(gear), GetY(gear), div(100*dx,dy), div(100*dy,dx))
   elseif GetGearType(gear) == gtHedgehog then
      if CurrentHedgehog ~= nil then
         local hog = hedgehogs[CurrentHedgehog]
         hog.hogsKilled = hog.hogsKilled + 1
         -- SpawnFakeAmmoCrate(0, 0, false, false)
 
         -- local target = AddGear(0, 0, gtTarget, 0, 0, 0, 0)
      -- FindPlace(target, true, 0, LAND_WIDTH)
      end
   elseif GetGearType(gear) == gtCase then
      hedgehogs[CurrentHedgehog].gotExtraPower = true
   end
end
 
function onNewTurn()
   if prevHog ~= nil then
      prevHog.gotExtraPower = false
   end
   prevHog = hedgehogs[CurrentHedgehog]
end
