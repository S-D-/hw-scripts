----------------------------------
-- Switch_In_Place_TS
--
-- The modification of The Specialists mode where hog specialities can be switched for the same hog before first attack.
-- by S.D.
----------------------------------

HedgewarsScriptLoad("/Scripts/Locale.lua")
HedgewarsScriptLoad("/Scripts/Tracker.lua")

local stTypeSwitch = true

local switchFuncs = {
   function ()
      SetHogHat(CurrentHedgehog, "Glasses")
      local newName = "Engineer"
      addSwitchCaption(newName)
      SetHogName(CurrentHedgehog, newName)

      AddAmmo(CurrentHedgehog, amBazooka, 0)
      AddAmmo(CurrentHedgehog, amGrenade, 0)
      AddAmmo(CurrentHedgehog, amShotgun, 0)
      
      AddAmmo(CurrentHedgehog, amGirder, 2)
      AddAmmo(CurrentHedgehog, amBlowTorch, 1)
      AddAmmo(CurrentHedgehog, amPickHammer, 1)
   end
   ,
   function ()
      local newName = "Ninja"
      addSwitchCaption(newName)
      SetHogName(CurrentHedgehog, newName)
      SetHogHat(CurrentHedgehog, "NinjaFull")

      AddAmmo(CurrentHedgehog, amGirder, 0)
      AddAmmo(CurrentHedgehog, amBlowTorch, 0)
      AddAmmo(CurrentHedgehog, amPickHammer, 0)
      
      AddAmmo(CurrentHedgehog, amRope, 100)
      AddAmmo(CurrentHedgehog, amParachute, 9)
      AddAmmo(CurrentHedgehog, amFirePunch, 1)
   end
   ,
   function ()
      local newName = "Demo"
      addSwitchCaption(newName)
      SetHogName(CurrentHedgehog, newName)
      SetHogHat(CurrentHedgehog, "Skull")

      AddAmmo(CurrentHedgehog, amRope, 0)
      AddAmmo(CurrentHedgehog, amParachute, 0)
      AddAmmo(CurrentHedgehog, amFirePunch, 0)

      AddAmmo(CurrentHedgehog, amDynamite, 1)
      AddAmmo(CurrentHedgehog, amMine, 1)
      AddAmmo(CurrentHedgehog, amDrill, 1)
   end
   ,
   function ()
      local newName = "Sniper"
      addSwitchCaption(newName)
      SetHogName(CurrentHedgehog, newName)
      SetHogHat(CurrentHedgehog, "Sniper")

      AddAmmo(CurrentHedgehog, amDynamite, 0)
      AddAmmo(CurrentHedgehog, amMine, 0)
      AddAmmo(CurrentHedgehog, amDrill, 0)
      
      AddAmmo(CurrentHedgehog, amSniperRifle, 1)
      AddAmmo(CurrentHedgehog, amDEagle, 1)
      AddAmmo(CurrentHedgehog, amPortalGun, 2)
   end
   ,
   function ()
      local newName = "Saint"
      addSwitchCaption(newName)
      SetHogName(CurrentHedgehog, newName)
      SetHogHat(CurrentHedgehog, "angel")

      AddAmmo(CurrentHedgehog, amSniperRifle, 0)
      AddAmmo(CurrentHedgehog, amDEagle, 0)
      AddAmmo(CurrentHedgehog, amPortalGun, 0)

      AddAmmo(CurrentHedgehog, amSeduction, 9)
      AddAmmo(CurrentHedgehog, amResurrector, 1)
      AddAmmo(CurrentHedgehog, amInvulnerable, 1)
   end
   ,
   function ()
      local newName = "Pyro"
      addSwitchCaption(newName)
      SetHogName(CurrentHedgehog, newName)
      SetHogHat(CurrentHedgehog, "Gasmask")
      
      AddAmmo(CurrentHedgehog, amSeduction, 0)
      AddAmmo(CurrentHedgehog, amResurrector, 0)
      AddAmmo(CurrentHedgehog, amInvulnerable, 0)

      AddAmmo(CurrentHedgehog, amFlamethrower, 1)
      AddAmmo(CurrentHedgehog, amMolotov, 1)
      AddAmmo(CurrentHedgehog, amNapalm, 1)
   end
   ,
   function ()
      local newName = "Loon"
      addSwitchCaption(newName)
      SetHogName(CurrentHedgehog, newName)
      SetHogHat(CurrentHedgehog, "clown")

      AddAmmo(CurrentHedgehog, amFlamethrower, 0)
      AddAmmo(CurrentHedgehog, amMolotov, 0)
      AddAmmo(CurrentHedgehog, amNapalm, 0)

      AddAmmo(CurrentHedgehog, amBaseballBat, 1)
      AddAmmo(CurrentHedgehog, amGasBomb, 1)
      AddAmmo(CurrentHedgehog, amKamikaze, 1)
   end
   ,
   function ()
      local newName = "Soldier"
      addSwitchCaption(newName)
      SetHogName(CurrentHedgehog, newName)
      SetHogHat(CurrentHedgehog, "sf_vega")

      AddAmmo(CurrentHedgehog, amBaseballBat, 0)
      AddAmmo(CurrentHedgehog, amGasBomb, 0)
      AddAmmo(CurrentHedgehog, amKamikaze, 0)
      
      AddAmmo(CurrentHedgehog, amBazooka, 1)
      AddAmmo(CurrentHedgehog, amGrenade, 1)
      AddAmmo(CurrentHedgehog, amShotgun, 1)
   end
}

local hogTypeIdxs = {}
local hhs = {}

function addSwitchCaption(name)
   AddCaption(loc("Switched to ") .. name .. "!")
end

function onNewAmmoStore(groupIndex, hogIndex)
   SetAmmo(amSkip, 9, 0, 0, 0)
end

function onGameInit()
   ClearGameFlags()
   EnableGameFlags(gfRandomOrder, gfResetWeps, gfInfAttack, gfPlaceHog, gfPerHogAmmo)
   Delay = 10
   HealthCaseProb = 100
end

function onGameStart()

   for i, v in pairs(hhs) do
      SetHealth(hhs[i], 300)
   end

   ShowMission     (
      loc("THE SPECIALISTS. Switch in place."),
      loc("a Hedgewars mini-game"),

      loc("Eliminate the enemy specialists.") .. "|" ..
         " " .. "|" ..

         loc("Game Modifiers: ") .. "|" ..
         loc("Per-Hog Ammo") .. "|" ..
         loc("Weapons Reset") .. "|" ..
         loc("Unlimited Attacks") .. "|" ..

         "", 4, 4000
   )

   trackTeams()
   
end

function onNewTurn()
   stTypeSwitch = true
   if hogTypeIdxs[CurrentHedgehog] == nil then
      hogTypeIdxs[CurrentHedgehog] = 1
   end
   local idx = hogTypeIdxs[CurrentHedgehog]
   
   if idx ~= 1 then
      idx = idx - 1
   else
      idx = 8
   end
   switchFuncs[idx]()
end

function onGearAdd(gear)

   if GetGearType(gear) == gtHedgehog then
      hhs[#hhs+1] = gear
      trackGear(gear)
   elseif (GetGearType(gear) == gtMine) and (started == true) then
      SetTimer(gear,5000)
   elseif (GetGearType(gear) == gtResurrector) then
      trackGear(gear)
   end
   
end

function onGearDelete(gear)
   if (GetGearType(gear) == gtHedgehog) or (GetGearType(gear) == gtResurrector) then
      trackDeletion(gear)
   end
end

function onSwitch()
   if not stTypeSwitch then
      return
   end
   local curIdx = hogTypeIdxs[CurrentHedgehog]
   switchFuncs[curIdx]()
   hogTypeIdxs[CurrentHedgehog] = math.fmod(curIdx, #switchFuncs)+1
end

function onAttack()
   stTypeSwitch = false
end
