
HedgewarsScriptLoad("/Scriprs/Locale.lua")

local keyHash = ""

local curInput = {}

function str2bytes(str)
   local res = {}
   for i, v in pairs(str) do
      res[i] = string.byte(v)
   end
   return res
end

function copy(orig)
    local orig_type = type(orig)
    local res
    if orig_type == 'table' then
        res = {}
        for orig_key, orig_value in pairs(orig) do
            res[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        res = orig
    end
    return res
end

function rangeCopy(orig, b, e)
   res = {}
   local idx = 0
   for i = b, e do
      res[idx] = orig[i]
      idx = idx + 1
   end
   return res
end
   
--[[
Замечание: Все используемые переменные 32 бита.
function sha1(str)

   local data = str2bytes(str)
   
   Инициализация переменных:
   local h0 = 0x67452301
   local h1 = 0xEFCDAB89
   local h2 = 0x98BADCFE
   local h3 = 0x10325476
   local h4 = 0xC3D2E1F0

Предварительная обработка:
Присоединяем бит '1' к сообщению

Присоединяем k битов '0', где k наименьшее число ≥ 0 такое, что длина получившегося сообщения
(в битах) сравнима по модулю  512 с 448 (length mod 512 == 448)
Добавляем длину исходного сообщения (до предварительной обработки) как целое 64-битное
Big-endian число, в битах.
   data[#data] = 1
   while #data * 32 mod 512 ~= 448 do
      data[#data] = 0
   end
   data[#data] = #data-1
В процессе сообщение разбивается последовательно по 512 бит:
for перебираем все такие части
   for i = 1, #data, 512 do
    разбиваем этот кусок на 16 частей, слов по 32-бита w[i], 0 <= i <= 15
      for j = i, i+16 do
   local w = rangeCopy(data, i, i+16) //......
    16 слов по 32-бита дополняются до 80 32-битовых слов:
    for k = 16, 79 do
        w[k] = (w[k-3] xor w[k-8] xor w[k-14] xor w[k-16]) циклический сдвиг влево 1

    Инициализация хеш-значений этой части:
    local a = h0
    local b = h1
    local c = h2
    local d = h3
    local e = h4

    Основной цикл:
    for i2 = 0, 79
       local k
        if 0 <= i2 and i2 <= 19 then
            f = (b and c) or ((not b) and d)
            k = 0x5A827999
        else if 20 <= i2 and i2 <= 39 then
            f = b xor c xor d
            k = 0x6ED9EBA1
        else if 40 <= i2 and i2 <= 59 then
            f = (b and c) or (b and d) or (c and d)
            k = 0x8F1BBCDC
        else if 60 <= i2 and i2 <= 79 then
            f = b xor c xor d
            k = 0xCA62C1D6

        temp = (a leftrotate 5) + f + e + k + w[i2]
        e = d
        d = c
        c = b leftrotate 30
        b = a
        a = temp

    Добавляем хеш-значение этой части к результату:
    h0 = h0 + a
    h1 = h1 + b 
    h2 = h2 + c
    h3 = h3 + d
    h4 = h4 + e

Итоговое хеш-значение:
local digest = hash = h0 append h1 append h2 append h3 append h4
   return digest
]]--
